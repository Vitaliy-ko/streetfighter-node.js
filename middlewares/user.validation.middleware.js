const { user } = require('../models/user');
const validator = require('validator');

const {
  UserValidationError,
  UserIdValidationError
} = require('../utils/errors/userErrors');
const {
  getDataValidation,
  getIdValidation
} = require('./../utils/validation/index');

const skipFields = {
  id: ''
};

const validationRules = {
  firstName: '',
  lastName: '',
  email: email =>
    validator.isEmail(email) && validator.matches(email, /\.*(@gmail\.){1}/),
  phoneNumber: phoneNumber => validator.matches(phoneNumber, /^380[0-9]{9}$/),
  password: password => validator.isLength(password, { min: 3 })
};

const userDataValid = data => {
  const isUserDataValid = getDataValidation(
    data,
    user,
    skipFields,
    validationRules
  );
  if (!isUserDataValid) {
    throw new UserValidationError();
  }
};

const getUserIdValid = data => {
  const isIdValid = getIdValidation(data);
  if (!isIdValid) {
    throw new UserIdValidationError();
  }
};

const userIdValid = (req, res, next) => {
  try {
    getUserIdValid(req.params.id);
  } catch (err) {
    res.err = err;
  } finally {
    next();
  }
};

const createUserValid = (req, res, next) => {
  try {
    if (res.err) {
      throw res.err;
    }
    userDataValid(req.body);
  } catch (err) {
    res.err = err;
  } finally {
    next();
  }
};

const updateUserValid = (req, res, next) => {
  try {
    if (res.err) {
      throw res.err;
    }
    userDataValid(req.body);
  } catch (err) {
    res.err = err;
  } finally {
    next();
  }
};

exports.createUserValid = createUserValid;
exports.updateUserValid = updateUserValid;
exports.userIdValid = userIdValid;
