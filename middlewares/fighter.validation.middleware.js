const { fighter } = require('../models/fighter');
const validator = require('validator');
const {
  FighterValidationError,
  FighterIdValidationError
} = require('../utils/errors/fighterErrors');
const {
  getDataValidation,
  getIdValidation
} = require('./../utils/validation/index');

const skipFields = {
  id: ''
};

const validationRules = {
  name: name => validator.isLength(name, { min: 1 }),
  health: healthPoints => validator.isInt(healthPoints, { min: 1, max: 99 }),
  power: powerPoints => validator.isInt(powerPoints, { min: 1, max: 10 }),
  defense: defensePoints => validator.isInt(defensePoints, { min: 1, max: 10 })
};

const getFighterDataValid = data => {
  const isFighterDataValid = getDataValidation(
    data,
    fighter,
    skipFields,
    validationRules
  );

  if (!isFighterDataValid) {
    throw new FighterValidationError();
  }
};
const getFighterIdValid = id => {
  const isIdValid = getIdValidation(id);
  if (!isIdValid) {
    throw new FighterIdValidationError();
  }
};

const fighterIdValid = (req, res, next) => {
  try {
    getFighterIdValid(req.params.id);
  } catch (err) {
    res.err = err;
  } finally {
    next();
  }
};

const createFighterValid = (req, res, next) => {
  try {
    getFighterDataValid(req.body);
  } catch (err) {
    res.err = err;
  } finally {
    next();
  }
};

const updateFighterValid = (req, res, next) => {
  try {
    getFighterDataValid(req.body);
  } catch (err) {
    res.err = err;
  } finally {
    next();
  }
};

exports.createFighterValid = createFighterValid;
exports.updateFighterValid = updateFighterValid;
exports.fighterIdValid = fighterIdValid;
