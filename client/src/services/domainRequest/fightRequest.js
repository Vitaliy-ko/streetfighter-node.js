import { post } from '../requestHelper';

const entity = 'fights';

export const createFightLog = async body => {
  return await post(entity, body);
};
