import { createFightLog } from './domainRequest/fightRequest';

let fightData = {
  fighter1: '',
  fighter2: '',
  log: []
};

let logItem = {
  fighter1Shot: 0,
  fighter2Shot: 0,
  fighter1Health: 0,
  fighter2Health: 0
};

export function setInitialFightData(fighter1Id, fighter2Id) {
  fightData.fighter1 = fighter1Id;
  fightData.fighter2 = fighter2Id;
}

export function setInitialLogItem(fighter1Health, fighter2Health) {
  logItem = {
    ...logItem,
    fighter1Health,
    fighter2Health
  };
}

export function addToLog(attackerId, fighterShot, fighterHealth) {
  const { fighter1: fighter1Id, fighter2: fighter2Id } = fightData;
  const newLogItemData = new Map();

  newLogItemData.set(fighter1Id, {
    fighter1Shot: fighterShot,
    fighter2Shot: 0,
    fighter1Health: logItem.fighter1Health,
    fighter2Health: fighterHealth
  });
  newLogItemData.set(fighter2Id, {
    fighter1Shot: 0,
    fighter2Shot: fighterShot,
    fighter1Health: fighterHealth,
    fighter2Health: logItem.fighter2Health
  });

  logItem = { ...logItem, ...newLogItemData.get(attackerId) };

  const newLogItem = { ...logItem, ...newLogItemData.get(attackerId) };

  fightData.log.push(newLogItem);
}

export function createLog() {
  createFightLog(fightData);
}
