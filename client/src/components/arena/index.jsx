import React, { useRef, useState, useEffect } from 'react';
import './arena.css';
import { controls } from './../../constants/controls';
import { Winner } from './../modals/winner';
import {
  setInitialLogItem,
  setInitialFightData,
  addToLog,
  createLog
} from './../../services/fightLogHelper';

const firstFighterImgSrc =
  'https://media.giphy.com/media/kdHa4JvihB2gM/giphy.gif';
const secondFighterImgSrc =
  'https://i.pinimg.com/originals/46/4b/36/464b36a7aecd988e3c51e56a823dbedc.gif';

export const Arena = ({ selectedFighters, onFightEnd }) => {
  const [firstFighter, secondFighter] = selectedFighters;
  const { health: firstFighterHealth } = firstFighter;
  const { health: secondFighterHealth } = secondFighter;
  const firstFighterHPConst = Number(firstFighterHealth);
  const secondFighterHPConst = Number(secondFighterHealth);
  const criticalHitCombinationTime = 10000;
  const firstHealthIndicator = useRef(null);
  const secondHealthIndicator = useRef(null);
  const {
    PlayerOneAttack,
    PlayerTwoAttack,
    PlayerOneBlock,
    PlayerTwoBlock,
    PlayerOneCriticalHitCombination,
    PlayerTwoCriticalHitCombination
  } = controls;
  const [winner, setWinner] = useState(null);

  const [firstFighterHP, setFirstFighterHP] = useState(firstFighterHPConst);
  const [secondFighterHP, setSecondFighterHP] = useState(secondFighterHPConst);

  const [
    isFirstFighterCritCombDisabled,
    setIsFirstFighterCritCombDisabled
  ] = useState(false);
  const [
    isSecondFighterCritCombDisabled,
    setIsSecondFighterCritCombDisabled
  ] = useState(false);

  const keyEvents = new Set();

  useEffect(() => {
    setInitialLogItem(firstFighterHPConst, secondFighterHPConst);
    setInitialFightData(firstFighter.id, secondFighter.id);
  }, []);

  useEffect(() => {
    document.addEventListener('keydown', keydownHandler, false);
    document.addEventListener('keyup', keyupHandler, false);

    return () => {
      document.removeEventListener('keydown', keydownHandler);
      document.removeEventListener('keyup', keyupHandler);
    };
  });

  function keydownHandler(event) {
    keyEvents.add(event.code);
    fightAction();
  }
  function keyupHandler(event) {
    keyEvents.delete(event.code);
  }

  function fightAction() {
    damageAction();
    critDamageAction();
  }

  function damageAction() {
    const damageActions = new Map();

    const isPlayerOneDoDamage =
      keyEvents.has(PlayerOneAttack) &&
      !keyEvents.has(PlayerOneBlock) &&
      !keyEvents.has(PlayerTwoBlock);
    const isPlayerTwoDoDamage =
      keyEvents.has(PlayerTwoAttack) &&
      !keyEvents.has(PlayerTwoBlock) &&
      !keyEvents.has(PlayerOneBlock);

    damageActions.set(isPlayerOneDoDamage, [
      firstFighter,
      firstFighterImgSrc,
      secondFighter,
      secondFighterHP,
      secondHealthIndicator,
      secondFighterHPConst,
      setSecondFighterHP
    ]);

    damageActions.set(isPlayerTwoDoDamage, [
      secondFighter,
      secondFighterImgSrc,
      firstFighter,
      firstFighterHP,
      firstHealthIndicator,
      firstFighterHPConst,
      setFirstFighterHP
    ]);

    const damageData = damageActions.get(true);
    if (damageData) {
      doDamage(damageData);
    }
  }

  function critDamageAction() {
    const critDamageActions = new Map();

    const isPlayerOneDoCritDamage =
      isCriticalHitCombination(PlayerOneCriticalHitCombination) === true &&
      isFirstFighterCritCombDisabled === false;
    const isPlayerTwoDoCritDamage =
      isCriticalHitCombination(PlayerTwoCriticalHitCombination) === true &&
      isSecondFighterCritCombDisabled === false;

    critDamageActions.set(isPlayerOneDoCritDamage, [
      firstFighter,
      firstFighterImgSrc,
      secondFighterHP,
      secondFighterHPConst,
      secondHealthIndicator,
      setSecondFighterHP,
      setIsFirstFighterCritCombDisabled
    ]);

    critDamageActions.set(isPlayerTwoDoCritDamage, [
      secondFighter,
      secondFighterImgSrc,
      firstFighterHP,
      firstFighterHPConst,
      firstHealthIndicator,
      setFirstFighterHP,
      setIsSecondFighterCritCombDisabled
    ]);

    const critDamageData = critDamageActions.get(true);
    if (critDamageData) {
      doCritDamage(critDamageData);
    }
  }

  function getBlockPower(fighter) {
    const { defense } = fighter;
    const dodgeChance = getChance();
    const power = defense * dodgeChance;
    return power;
  }

  function getHitPower(fighter) {
    const { power } = fighter;
    const criticalHitChance = getChance();
    const result = power * criticalHitChance;
    return result;
  }

  function getDamage(attacker, defender) {
    const damage = getHitPower(attacker) - getBlockPower(defender);
    return Math.max(0, damage);
  }

  function doDamage([
    attacker,
    attackerImgSrc,
    defender,
    defenderHP,
    defenderHealthIndicator,
    defenderHPConst,
    setDefenderHP
  ]) {
    const damage = getDamage(attacker, defender);
    const defenderHealthLeft = defenderHP - damage;
    changeHPIndicator(
      defenderHealthLeft,
      defenderHPConst,
      defenderHealthIndicator
    );
    addToLog(attacker.id, damage, defenderHealthLeft);

    checkForWinner(attacker, defenderHealthLeft, attackerImgSrc);

    setDefenderHP(defenderHealthLeft);
  }

  function changeHPIndicator(
    defenderHP,
    defenderHPConst,
    defenderHealthIndicator
  ) {
    defenderHealthIndicator.current.style.width =
      Math.max(0, (defenderHP / defenderHPConst) * 100) + '%';
  }

  function doCritDamage([
    attacker,
    attackerImgSrc,
    defenderHP,
    defenderHPConst,
    defenderHealthIndicator,
    setDefenderHP,
    setIsAttackerCritCombDisabled
  ]) {
    const damage = getCombinationDamage(attacker);
    const defenderHealthLeft = defenderHP - damage;

    changeHPIndicator(
      defenderHealthLeft,
      defenderHPConst,
      defenderHealthIndicator
    );

    addToLog(attacker.id, damage, defenderHealthLeft);
    checkForWinner(attacker, defenderHealthLeft, attackerImgSrc);

    setDefenderHP(defenderHealthLeft);

    setIsAttackerCritCombDisabled(true);
    setTimeout(
      () => setIsAttackerCritCombDisabled(false),
      criticalHitCombinationTime
    );
  }

  function getCombinationDamage(fighter) {
    const { power } = fighter;
    return power * 2;
  }

  function getChance() {
    return Math.random() + 1;
  }

  function isCriticalHitCombination(combination) {
    return combination.every(code => keyEvents.has(code));
  }

  function checkForWinner(attacker, defenderHealth, imgSrc) {
    if (defenderHealth <= 0) {
      document.removeEventListener('keydown', keydownHandler);
      document.removeEventListener('keyup', keyupHandler);
      createLog();
      setWinner(
        <Winner imgSrc={imgSrc} fighter={attacker} closeModal={onFightEnd} />
      );
    }
  }

  return (
    <>
      {winner}
      <div className={'arena___root'}>
        <div className={'arena___fight-status'}>
          {selectedFighters.map((fighter, index) => (
            <div key={index} className={'arena___fighter-indicator'}>
              <span className={'arena___fighter-name'}>{fighter.name}</span>
              <div className={'arena___health-indicator'}>
                <div
                  className={'arena___health-bar'}
                  ref={
                    index === 0 ? firstHealthIndicator : secondHealthIndicator
                  }
                  id={[
                    index === 0 ? 'left' : 'right',
                    '-fighter-indicator'
                  ].join('')}
                ></div>
              </div>
            </div>
          ))}
        </div>
        <div className={'arena___battlefield'}>
          {selectedFighters.map((fighter, index) => (
            <div
              key={index}
              className={[
                'arena___fighter',
                index === 0 ? 'arena___left-fighter' : 'arena___right-fighter'
              ].join(' ')}
            >
              <img
                src={index === 0 ? firstFighterImgSrc : secondFighterImgSrc}
                alt={'fighter' + (index + 1)}
                className={'fighter-preview___img'}
              />
            </div>
          ))}
        </div>
      </div>
    </>
  );
};
