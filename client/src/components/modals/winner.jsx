import React from 'react';
import './modal.css';

export const Winner = ({ imgSrc, fighter, closeModal }) => (
  <div className={'modal-layer'}>
    <div className={'modal-root'}>
      <div className={'modal-header'}>
        <span>Winner</span>
        <button onClick={closeModal} className={'close-btn'}>×</button>
      </div>
      <div className={'modal-body'}>
        <p className={'fighter-name'}>{fighter.name}</p>
        <img
          className={'fighter-preview___img'}
          src={imgSrc}
          title={fighter.name}
          alt={fighter.name}
        />
      </div>
    </div>
  </div>
);
