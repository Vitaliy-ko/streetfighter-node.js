const { FighterRepository } = require('../repositories/fighterRepository');
const {
  FighterConflictError,
  FighterNotFoundError
} = require('./../utils/errors/fighterErrors');

class FighterService {
  async createFighter(fighter) {
    const { name } = fighter;
    const isFighterExists = this.search({ name });

    if (isFighterExists) {
      throw new FighterConflictError();
    }
    const createdFighter = await FighterRepository.create(fighter);
    return { payload: createdFighter, status: 200 }; //201
  }

  async updateFighter(id, dataToUpdate) {
    const fighter = await this.search({ id });

    if (!fighter) {
      throw new FighterNotFoundError();
    }

    const { name } = dataToUpdate;
    const anotherFighter = await this.search({ name });
    if (fighter.name !== dataToUpdate.name && anotherFighter) {
      throw new FighterConflictError();
    }

    const updatedFighter = await FighterRepository.update(id, dataToUpdate);
    return { payload: updatedFighter, status: 200 };
  }

  async getFighter(id) {
    const fighter = await this.search({ id });
    if (!fighter) {
      throw new FighterNotFoundError();
    }
    return { payload: fighter, status: 200 };
  }

  async getFighters() {
    const fighters = await FighterRepository.getAll();
    return { payload: fighters, status: 200 };
  }

  async deleteFighter(id) {
    const deletedFighter = await FighterRepository.delete(id);
    if (!deletedFighter.length) {
      throw new FighterNotFoundError();
    }
    return { payload: deletedFighter, status: 200 };
  }

  search(search) {
    const item = FighterRepository.getOne(search);
    if (!item) {
      return null;
    }
    return item;
  }
}

module.exports = new FighterService();
