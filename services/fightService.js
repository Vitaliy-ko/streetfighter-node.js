const { FightRepository } = require('../repositories/fightRepository');
const { FightLogNotFoundError } = require('./../utils/errors/fightLogErrors');

class FightersService {
  async createFightLog(fightLogData) {
    const createdFightLog = await FightRepository.create(fightLogData);
    return { payload: createdFightLog, status: 200 }; //201
  }

  async getFightLog(id) {
    const fightLog = await this.search({ id });
    if (!fightLog) {
      throw new FightLogNotFoundError();
    }
    return { payload: fightLog, status: 200 };
  }

  async getFightsLogs() {
    const fightsLogs = await FightRepository.getAll();
    return { payload: fightsLogs, status: 200 };
  }

  async deleteFightLog(id) {
    const deletedFightLog = await FightRepository.delete(id);
    if (!deletedFightLog.length) {
      throw new FightLogNotFoundError();
    }
    return { payload: deletedFightLog, status: 200 };
  }

  search(search) {
    const item = FightRepository.getOne(search);
    if (!item) {
      return null;
    }
    return item;
  }
}

module.exports = new FightersService();
