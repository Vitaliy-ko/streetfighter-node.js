class CustomError extends Error {
  constructor(message) {
    super(message);
    this.name = this.constructor.name;
    this.error = true;
  }
}

exports.ValidationError = class ValidationError extends CustomError {
  constructor(message = 'Invalid data') {
    super(message);
    this.status = 400;
  }
};

exports.NotFoundError = class NotFoundError extends CustomError {
  constructor(message = 'Not Found') {
    super(message);
    this.status = 404;
  }
};

exports.ServerError = class ServerError extends CustomError {
  constructor(message = 'Internal Server Error') {
    super(message);
    this.status = 500;
  }
};
