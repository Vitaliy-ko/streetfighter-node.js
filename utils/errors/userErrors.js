const { ValidationError, NotFoundError } = require('./baseErrors');

class UserValidationError extends ValidationError {
  constructor(message = 'Invalid user data') {
    super(message);
    this.status = 400;
  }
}

class UserIdValidationError extends UserValidationError {
  constructor(message = 'Invalid user id') {
    super(message);
  }
}

class UserNotFoundError extends NotFoundError {
  constructor(message = 'User not Found') {
    super(message);
    this.status = 404;
  }
}

class UserConflictError extends ValidationError {
  constructor(
    message = 'A user with this email address or phone number is already registered'
  ) {
    super(message);
    this.status = 400; //409
  }
}

exports.UserValidationError = UserValidationError;
exports.UserIdValidationError = UserIdValidationError;
exports.UserNotFoundError = UserNotFoundError;
exports.UserConflictError = UserConflictError;
