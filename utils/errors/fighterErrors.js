const { ValidationError, NotFoundError } = require('./baseErrors');

class FighterValidationError extends ValidationError {
  constructor(message = 'Invalid fighter data') {
    super(message);
  }
}

class FighterIdValidationError extends ValidationError {
  constructor(message = 'Invalid fighter id') {
    super(message);
  }
}

class FighterNotFoundError extends NotFoundError {
  constructor(message = 'Fighter not Found') {
    super(message);
  }
}

class FighterConflictError extends ValidationError {
  constructor(message = 'A fighter with this name is already exists') {
    super(message);
    this.status = 400; //409
  }
}

exports.FighterValidationError = FighterValidationError;
exports.FighterIdValidationError = FighterIdValidationError;
exports.FighterNotFoundError = FighterNotFoundError;
exports.FighterConflictError = FighterConflictError;
