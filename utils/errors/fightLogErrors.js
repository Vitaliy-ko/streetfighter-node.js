const { NotFoundError } = require('./baseErrors');

class FightLogNotFoundError extends NotFoundError {
  constructor(message = 'Fighter not Found') {
    super(message);
  }
}

exports.FightLogNotFoundError = FightLogNotFoundError;
