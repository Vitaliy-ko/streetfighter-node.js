const validator = require('validator');

const getDataValidation = (data, model, skipFields, validationRules) => {
  const dataProps = Object.keys(data);
  const requiredFields = { ...validationRules };
  let isValid = dataProps.every(prop => {
    const isPropAllowed =
      model.hasOwnProperty(prop) && !skipFields.hasOwnProperty(prop);

    if (!isPropAllowed) {
      return false;
    }

    const isPropRequiresValidation = validationRules.hasOwnProperty(prop);
    if (isPropRequiresValidation) {
      delete requiredFields[prop];
      if (typeof validationRules[prop] === 'function') {
        return validationRules[prop](data[prop]);
      }
      return true;
    }

    return true;
  });

  if (Object.keys(requiredFields).length) {
    isValid = false;
  }

  return isValid;
};

const getIdValidation = id => {
  return validator.isUUID(id, 4);
};

exports.getDataValidation = getDataValidation;
exports.getIdValidation = getIdValidation;
