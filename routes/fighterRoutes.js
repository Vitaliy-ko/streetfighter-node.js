const { Router } = require('express');
const FighterService = require('../services/fighterService');
const { responseMiddleware } = require('../middlewares/response.middleware');
const {
  createFighterValid,
  updateFighterValid,
  fighterIdValid
} = require('../middlewares/fighter.validation.middleware');

const router = Router();

router.post(
  '/',
  createFighterValid,
  async (req, res, next) => {
    try {
      if (res.err) {
        throw res.err;
      }
      const fighter = await FighterService.createFighter(req.body);
      res.data = fighter;
    } catch (err) {
      res.err = err;
    } finally {
      next();
    }
  },
  responseMiddleware
);

router.put(
  '/:id',
  updateFighterValid,
  async (req, res, next) => {
    try {
      if (res.err) {
        throw res.err;
      }
      const fighter = await FighterService.updateFighter(
        req.params.id,
        req.body
      );
      res.data = fighter;
    } catch (err) {
      res.err = err;
    } finally {
      next();
    }
  },
  responseMiddleware
);

router.get(
  '/:id',
  async (req, res, next) => {
    try {
      if (res.err) {
        throw res.err;
      }
      const { id } = req.params;
      const fighter = await FighterService.getFighter(id);
      res.data = fighter;
    } catch (err) {
      res.err = err;
    } finally {
      next();
    }
  },
  responseMiddleware
);

router.get(
  '/',
  async (req, res, next) => {
    try {
      if (res.err) {
        throw res.err;
      }
      const fighters = await FighterService.getFighters();
      res.data = fighters;
    } catch (err) {
      res.err = err;
    } finally {
      next();
    }
  },
  responseMiddleware
);

router.delete(
  '/:id',
  async (req, res, next) => {
    try {
      if (res.err) {
        throw res.err;
      }
      const { id } = req.params;
      const fighter = await FighterService.deleteFighter(id);
      res.data = fighter;
    } catch (err) {
      res.err = err;
    } finally {
      next();
    }
  },
  responseMiddleware
);

module.exports = router;
