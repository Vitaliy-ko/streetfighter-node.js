const { Router } = require('express');
const UserService = require('../services/userService');
const {
  updateUserValid,
  userIdValid,
  createUserValid
} = require('../middlewares/user.validation.middleware');
const { responseMiddleware } = require('../middlewares/response.middleware');

const router = Router();

router.post(
  '/',
  createUserValid,
  async (req, res, next) => {
    try {
      if (res.err) {
        throw res.err;
      }

      const user = await UserService.createUser(req.body);
      res.data = user;
    } catch (err) {
      res.err = err;
    } finally {
      next();
    }
  },
  responseMiddleware
);

router.put(
  '/:id',
  updateUserValid,
  async (req, res, next) => {
    try {
      if (res.err) {
        throw res.err;
      }

      const { id } = req.params;
      const { body } = req;
      const updatedUser = await UserService.updateUser(id, body);
      res.data = updatedUser;
    } catch (err) {
      res.err = err;
    } finally {
      next();
    }
  },
  responseMiddleware
);

router.get(
  '/:id',
  async (req, res, next) => {
    try {
      if (res.err) {
        throw res.err;
      }

      const { id } = req.params;
      const user = await UserService.getUser(id);
      res.data = user;
    } catch (err) {
      res.err = err;
    } finally {
      next();
    }
  },
  responseMiddleware
);

router.get(
  '/',
  async (req, res, next) => {
    try {
      const users = await UserService.getUsers();
      res.data = users;
    } catch (err) {
      res.err = err;
    } finally {
      next();
    }
  },
  responseMiddleware
);

router.delete(
  '/:id',
  async (req, res, next) => {
    try {
      if (res.err) {
        throw res.err;
      }

      const { id } = req.params;
      const deletedUser = await UserService.deleteUser(id);
      res.data = deletedUser;
    } catch (err) {
      res.err = err;
    } finally {
      next();
    }
  },
  responseMiddleware
);

module.exports = router;
