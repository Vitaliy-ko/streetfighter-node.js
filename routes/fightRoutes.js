const { Router } = require('express');
const FightService = require('../services/fightService');
const { responseMiddleware } = require('../middlewares/response.middleware');

const router = Router();

router.post(
  '/',
  async (req, res, next) => {
    try {
      const fightLog = await FightService.createFightLog(req.body);
      res.data = fightLog;
    } catch (err) {
      res.err = err;
    } finally {
      next();
    }
  },
  responseMiddleware
);

module.exports = router;
